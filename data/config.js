var lightName = document.getElementById('name'),
    mdnsName = document.getElementById('mdnsName'),
    mqttEnable = document.getElementById('mqttEnable'),
    mqttDiv = document.getElementById('mqttDiv'),
    mqttServer = document.getElementById('mqttServer'),
    mqttUser = document.getElementById('mqttUser'),
    mqttPassword = document.getElementById('mqttPassword'),
    mqttCommandTopic = document.getElementById('mqttCommandTopic'),
    mqttStateTopic = document.getElementById('mqttStateTopic');

mqttEnable.onchange = function() {
  mqttDiv.style.display = (this.checked ? 'block' : 'none');
};

getSettings = function()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var settings = xhttp.responseText.split(",");
        lightName.value = settings[0];
        mdnsName.value = settings[1];
        mqttServer.value = settings[2];
        mqttUser.value = settings[3];
        mqttPassword.value = settings[4];
        mqttEnable.checked = (settings[5] == "1" ? true : false);
        mqttEnable.value = (settings[5] == "1" ? 1 : 0);
        mqttCommandTopic.innerHTML = "<h6>light/" + settings[1] + "/command</h6>";
        mqttStateTopic.innerHTML = "<h6>light/" + settings[1] + "/state</h6>";

        mqttDiv.style.display = (mqttEnable.checked ? 'block' : 'none');
      }
    }
  };

  xhttp.open("GET", "getsettings", true);
  xhttp.send();
}

// To get around onLoad not firing on back
setTimeout(getSettings, 100);

$('input[type="checkbox"]').change(function(){ this.value = this.checked ? 1 : 0; });