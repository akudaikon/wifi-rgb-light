/*
 * ----------------------------------------------------------------------------------
 *  ESP-12E Pinout:
 * ----------------------------------------------------------------------------------
 *               ------------------------
 *             --| Reset       D1 (TX0) |-- Serial TX/Prog RX
 *             --| ADC         D3 (RX0) |-- Serial RX/Prog TX/LEDs
 *         VCC --| CHPD        D4 (SCL) |--
 *             --| D16         D5 (SDA) |--
 *             --| D14 (SCK)         D0 |-- Bootloader (low - program, high - normal)
 *             --| D12 (MISO)  D2 (TX1) |--
 *             --| D13 (MOSI)  D15 (SS) |-- GND (for normal startup)
 *             --| VCC              GND |--
 *               ------------------------
 * ----------------------------------------------------------------------------------
 */

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServerSPIFFS.h>
#include <WiFiManager.h>
#include <DNSServer.h>
#include <WiFiClient.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <EEPROM.h>
#include <FS.h>
#include <NeoPixelBus.h>

#define PIXEL_PIN           3     // Must be RX0 - LEDs DMA driven (via I2S)
#define NUM_PIXELS          64

#define FADE_TIME           10

#define SETTINGS_REV        0xA1
#define MAX_STRING_LENGTH   128

enum EEPROMSettings
{
  SETTING_INITIALIZED,
  SETTING_MQTT_ENABLE,
  SETTING_DELIM,
  // Max 128 characters for EEPROM stored strings below...
  SETTING_NAME_LENGTH,
  SETTING_NAME,
  SETTING_MDNS_LENGTH = SETTING_NAME + MAX_STRING_LENGTH,
  SETTING_MDNS,
  SETTING_MQTT_SERVER_LENGTH = SETTING_MDNS + MAX_STRING_LENGTH,
  SETTING_MQTT_SERVER,
  SETTING_MQTT_USER_LENGTH = SETTING_MQTT_SERVER + MAX_STRING_LENGTH,
  SETTING_MQTT_USER,
  SETTING_MQTT_PASSWORD_LENGTH = SETTING_MQTT_USER + MAX_STRING_LENGTH,
  SETTING_MQTT_PASSWORD,
  NUM_OF_SETTINGS = SETTING_MQTT_PASSWORD + MAX_STRING_LENGTH
};

String lightName = "RGB Light";
String mdnsName;

MDNSResponder mdns;
ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;

bool mqttEnable = false;
String mqttServer = "";
String mqttUser = "";
String mqttPassword = "";
WiFiClient mqttClient;
PubSubClient mqtt(mqttClient);

NeoGamma<NeoGammaTableMethod> colorGamma;
NeoPixelBus<NeoGrbwFeature, Neo800KbpsMethod> pixels(NUM_PIXELS, PIXEL_PIN);

RgbwColor currentColor = RgbwColor(0, 0, 0, 0);
RgbwColor targetColor = RgbwColor(0, 0, 0, 0);

enum { IDLE, FADE, EFFECT };
uint8_t state = IDLE;

enum { NONE, RAINBOW, NUM_EFFECTS };
const char* effectNames[] = { "None", "Rainbow" };
uint8_t currentEffect = NONE;
uint8_t effectState = 0;

long lastLEDUpdate = 0;
float fadeProgress = 0.0;

void setup()
{
  // Initialize LEDs
  pixels.Begin();
  for (int i = 0; i < NUM_PIXELS; i++) pixels.SetPixelColor(i, currentColor);
  pixels.Show();

  // Get MAC address and build AP SSID
  uint8_t macAddr[6];
  char apName[13];
  WiFi.macAddress(macAddr);
  sprintf(apName, "RGBLight-%02x%02x%02x", macAddr[3], macAddr[4], macAddr[5]);
  mdnsName = apName;

  // Initialize and read EEPROM settings
  EEPROM.begin(NUM_OF_SETTINGS);
  readEEPROMSettings();

  // Initialize SPIFFS
  SPIFFS.begin();

  // Connect to WiFi
  WiFiManager wifiManager;
  if (!wifiManager.autoConnect(apName)) ESP.reset();

  // Start servers
  mdns.begin(mdnsName.c_str());
  server.on("/on", []() { targetColor = RgbwColor(0, 0, 0, 255); fadeProgress = 0; state = FADE; server.send(200); });
  server.on("/off", []() { targetColor = RgbwColor(0, 0, 0, 0); fadeProgress = 0; state = FADE; server.send(200); });
  server.on("/status", sendStatus);
  server.on("/settings", HTTP_GET, []() { handleFileRead("/config.htm"); });
  server.on("/settings", HTTP_POST, saveSettings);
  server.on("/getsettings", getSettings);
  server.on("/reset", [&wifiManager]()
  {
    wifiManager.resetSettings();
    EEPROM.write(SETTING_INITIALIZED, 0);
    EEPROM.commit();
    server.send(200, "text/html", F("Clearing all saved settings (including Wifi) and restarting..."));
    delay(3000);
    ESP.restart();
  });
  server.on("/restart", []()
  {
    server.send(200, "text/html", F("<META http-equiv=\"refresh\" content=\"10;URL='/'\">Restarting..."));
    delay(3000);
    ESP.restart();
  });
  server.onNotFound([]()
  {
    if (!handleFileRead(server.uri())) server.send(404, "text/plain", "Not Found");
  });
  httpUpdater.setup(&server);
  server.begin();

  // Setup MQTT, if enabled
  if (mqttEnable)
  {
    mqtt.setServer(mqttServer.c_str(), 1883);
    mqtt.setCallback(mqttCallback);
    mqttReconnect();
  }
}

void loop()
{
  // Handle MQTT, if enabled
  if (mqttEnable)
  {
    if (!mqtt.connected()) mqttReconnect();
    if (mqtt.connected()) mqtt.loop();
  }

  // Handle HTTP
  server.handleClient();

  switch(state)
  {
    case IDLE: break;

    case FADE:
      if (millis() - lastLEDUpdate > FADE_TIME)
      {
        RgbwColor newColor = RgbwColor::LinearBlend(currentColor, targetColor, fadeProgress);
        for (int i = 0; i < NUM_PIXELS; i++) pixels.SetPixelColor(i, newColor);
        pixels.Show();

        fadeProgress += 0.01;
        if (fadeProgress > 1.0)
        {
          currentColor = targetColor;
          state = IDLE;
        }
        lastLEDUpdate = millis();
      }
    break;

    case EFFECT:
      switch(currentEffect)
      {
        case NONE: break;

        case RAINBOW:
          if (millis() - lastLEDUpdate > 10)
          {
            for (uint8_t i = 0; i < NUM_PIXELS; i++) 
              pixels.SetPixelColor(i, colorGamma.Correct(Wheel(((i * 256 / NUM_PIXELS) + effectState) % 256)));

            pixels.Show();
            effectState = (effectState + 3) % 256;
            lastLEDUpdate = millis();
          }
        break;

        // TODO: add more effects!
      }
    break;
  }
}

void initEEPROMSettings()
{
  EEPROM.write(SETTING_INITIALIZED, SETTINGS_REV);  // EEPROM initialized
  EEPROM.write(SETTING_MQTT_ENABLE,            0);  // Enable MQTT
  EEPROM.write(SETTING_DELIM,                  0);  // Dummy setting to delimit start of strings
  writeEEPROMString(SETTING_NAME, SETTING_NAME_LENGTH, lightName);
  writeEEPROMString(SETTING_MDNS, SETTING_MDNS_LENGTH, mdnsName);
  writeEEPROMString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH, "");
  writeEEPROMString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH, "");
  writeEEPROMString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH, "");
  EEPROM.commit();
}

void readEEPROMSettings()
{
  if (EEPROM.read(SETTING_INITIALIZED) != SETTINGS_REV) initEEPROMSettings();

  lightName = readEEPROMString(SETTING_NAME, SETTING_NAME_LENGTH);
  mdnsName = readEEPROMString(SETTING_MDNS, SETTING_MDNS_LENGTH);
  mqttEnable = EEPROM.read(SETTING_MQTT_ENABLE);
  mqttServer = readEEPROMString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH);
  mqttUser = readEEPROMString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH);
  mqttPassword = readEEPROMString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH);
}

String readEEPROMString(uint16_t startAddress, uint16_t lengthAddress)
{
  String returnString = "";
  uint8_t length = constrain(EEPROM.read(lengthAddress), 0, MAX_STRING_LENGTH);
  for (int i = 0; i < length; i++) returnString += (char)EEPROM.read(startAddress + i);
  return returnString;
}

void writeEEPROMString(uint16_t startAddress, uint16_t lengthAddress, String str)
{
  uint8_t length = constrain(str.length(), 0, MAX_STRING_LENGTH);
  EEPROM.write(lengthAddress, length);
  for (int i = 0; i < length; i++) EEPROM.write(startAddress + i, str[i]);
  EEPROM.commit();
}

void sendStatus()
{
  String response;
  IPAddress ip = WiFi.localIP();

  response = lightName;
  response += ",";
  response += (currentColor.R || currentColor.G || currentColor.B || currentColor.W || currentEffect) ? "1" : "0";
  response += ",";
  response += mdnsName;
  response += ",";
  response += mqtt.connected();
  response += ",";
  response += String(ip[0]) + "." + String(ip[1]) + "." + String(ip[2]) + "." + String(ip[3]);

  server.sendHeader("Cache-Control", "no-cache");
  server.send(200, "text/html", response);
}

void saveSettings()
{
  String response;

  if (server.hasArg("name"))
  {
    lightName = server.arg("name");
    writeEEPROMString(SETTING_NAME, SETTING_NAME_LENGTH, lightName);
  }
  if (server.hasArg("mdnsName"))
  {
    mdnsName = server.arg("mdnsName");
    writeEEPROMString(SETTING_MDNS, SETTING_MDNS_LENGTH, mdnsName);
  }
  if (server.hasArg("mqttEnable"))
  {
    mqttEnable = (server.arg("mqttEnable").toInt() ? true : false);
    EEPROM.write(SETTING_MQTT_ENABLE, mqttEnable);
  }
  if (server.hasArg("mqttServer"))
  {
    mqttServer = server.arg("mqttServer");
    writeEEPROMString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH, mqttServer);
  }
  if (server.hasArg("mqttUser"))
  {
    mqttUser = server.arg("mqttUser");
    writeEEPROMString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH, mqttUser);
  }
  if (server.hasArg("mqttPassword"))
  {
    mqttPassword = server.arg("mqttPassword");
    writeEEPROMString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH, mqttPassword);
  }
  EEPROM.commit();

  response = F("<META http-equiv=\"refresh\" content=\"10;URL='/settings'\">Settings saved! Restarting to take effect...<br><br>");
  server.send(200, "text/html", response);
  delay(1000);
  ESP.reset();
}

void getSettings()
{
  String response;
  response = lightName;
  response += ",";
  response += mdnsName;
  response += ",";
  response += mqttServer;
  response += ",";
  response += mqttUser;
  response += ",";
  response += mqttPassword;
  response += ",";

  for (uint8_t i = 1; i < SETTING_DELIM; i++)
  {
    response += EEPROM.read(i);
    response += ",";
  }

  server.sendHeader("Cache-Control", "no-cache");
  server.send(200, "text/plain", response);
}

bool handleFileRead(String path)
{
  if (path.endsWith("/")) path += "index.htm";

  String dataType = "text/plain";
  if (path.endsWith(".htm")) dataType = "text/html";
  else if (path.endsWith(".css")) dataType = "text/css";

  if (SPIFFS.exists(path))
  {
    File file = SPIFFS.open(path, "r");
    server.streamFile(file, dataType);
    file.close();
    return true;
  }
  return false;
}

void mqttReconnect()
{
  static long lastReconnect = 0;

  if (millis() - lastReconnect > 5000)
  {
    if (mqtt.connect(mdnsName.c_str(), mqttUser.c_str(), mqttPassword.c_str()))
    {
      String topic = "light/" + mdnsName + "/command";
      mqtt.subscribe(topic.c_str());

      DynamicJsonBuffer json;
      JsonObject &root = json.createObject();
      JsonObject& color = root.createNestedObject("color");
      
      root["state"] = (currentColor.R || currentColor.G || currentColor.B || currentColor.W || currentEffect) ? "ON" : "OFF";
      root["brightness"] = currentColor.W;
      color["r"] = currentColor.R;
      color["g"] = currentColor.G;
      color["b"] = currentColor.B;
      root["effect"] = effectNames[currentEffect];

      char buffer[root.measureLength() + 1];
      root.printTo(buffer, sizeof(buffer));
      topic = "light/" + mdnsName + "/state";
      mqtt.publish(topic.c_str(), buffer, true);

      lastReconnect = 0;
    }
    else lastReconnect = millis();
  }
}

void mqttCallback(char* topic, byte* payload, unsigned int length)
{
  char payload_assembled[length];
  for (int i = 0; i < length; i++) payload_assembled[i] = (char)payload[i];

  DynamicJsonBuffer json;
  JsonObject& root = json.parseObject(payload_assembled);

  if (!root.success())
  {
    Serial.println("Unable to parse JSON");
    return;
  }

  uint8_t r = 0;
  uint8_t g = 0;
  uint8_t b = 0;
  uint8_t w = 0;

  if (root.containsKey("effect"))
  {
    for (int i = 0; i < NUM_EFFECTS; i++)
    {
      if (!strcmp(root["effect"], effectNames[i]))
      {
        currentEffect = i;
        effectState = 0;
        state = EFFECT;
      }
    }
    if (currentEffect == NONE)
    {
      targetColor = RgbwColor(0, 0, 0, 0);
      currentColor = RgbwColor(0, 0, 0, 0);
      for (int i = 0; i < NUM_PIXELS; i++) pixels.SetPixelColor(i, currentColor);
      pixels.Show();
      state = IDLE;
    }
  }
  else
  {
    if (root.containsKey("state"))
    {
      if (!strcmp(root["state"], "ON"))
      {
        if (!root.containsKey("brightness") && !root.containsKey("color")) 
        {
          w = 255;
        }
      }
      else if (!strcmp(root["state"], "OFF"))
      {
        if (currentEffect)
        {
          targetColor = RgbwColor(0, 0, 0, 0);
          currentColor = RgbwColor(0, 0, 0, 0);
          for (int i = 0; i < NUM_PIXELS; i++) pixels.SetPixelColor(i, currentColor);
          pixels.Show();
          currentEffect = NONE;
          state = IDLE;
        }
      }
    }
    if (root.containsKey("color"))
    {
      r = root["color"]["r"];
      g = root["color"]["g"];
      b = root["color"]["b"];
      currentEffect = NONE;
    }
    if (root.containsKey("brightness"))
    {
      w = root["brightness"];
      currentEffect = NONE;
    }

    currentColor = pixels.GetPixelColor(0);
    targetColor = colorGamma.Correct(RgbwColor(r, g, b, w));

    if (currentColor != targetColor) 
    {
      fadeProgress = 0;
      state = FADE;
    }
  }

  json.clear();
  JsonObject& newRoot = json.createObject();
  JsonObject& newColor = newRoot.createNestedObject("color");

  newRoot["state"] = (r || g || b || w || currentEffect) ? "ON" : "OFF";
  newRoot["brightness"] = w;
  newColor["r"] = r;
  newColor["g"] = g;
  newColor["b"] = b;
  newRoot["effect"] = effectNames[currentEffect];

  char buffer[newRoot.measureLength() + 1];
  newRoot.printTo(buffer, sizeof(buffer));
  String pubTopic = "light/" + mdnsName + "/state";  
  mqtt.publish(pubTopic.c_str(), buffer, true);
}

RgbwColor Wheel(uint8_t WheelPos) 
{
  if (WheelPos < 85)
  {
    return RgbwColor(WheelPos * 3, 255 - WheelPos * 3, 0, 0);
  }
  else if (WheelPos < 170)
  {
    WheelPos -= 85;
    return RgbwColor(255 - WheelPos * 3, 0, WheelPos * 3, 0);
  } 
  else 
  {
    WheelPos -= 170;
    return RgbwColor(0, WheelPos * 3, 255 - WheelPos * 3, 0);
  }
}
